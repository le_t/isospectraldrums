Testing ToDO:
0. Create a testing parameters.txt. (DON'T forget the Neumann BD) x
1. Check if the reference mesh definition is correct. x
2. Hard-code the reference meshes for P2 and P3 elements
into separate creation functions. (double check!!!) x
3. Hard code the mass matrix for P2 and P3 elements w/o refinement. x
4. Hard-code the stiffness matrix for P2 and P3 elements w/o refinement. x
5. Hard-code the damping matrix for the reference rectangle
for P2 and P3 elements. x
7. Shift the mesh, generate the mass, stiffness and damping matrices
again and check those. ->Mass, Stiffness should not change, but
damping should. x

6. Test the shift function. 

8. Test P2 matrices with a refined mesh.
8.1 Mass x
8.2 Stiffness
8.3 Transport



NOTE: P2 and P3 basis require different triVert values.
Generate some cases which will fail, to test the tests!

-fopenmp -mtune=native -msse4.2 -ftree-vectorize -fomit-frame-pointer -ffunction-sections