P3 BASIS!!!


Errors A+C (with iter ref):
#############################################
Iterative refinement
Refinement : 0
h :	1
L2 :	0.00259013
H1 :	0.0284142
f :	0.466669

Iterative refinement
Refinement : 1
h :	0.5
L2 :	0.000735367
H1 :	0.00771485
f :	0.466661

Iterative refinement
Refinement : 2
h :	0.25
L2 :	0.000658852
H1 :	0.00425353
f :	0.466661

Iterative refinement
Refinement : 3
h :	0.125
L2 :	0.0006583
H1 :	0.00415761
f :	0.466661

Iterative refinement
Refinement : 4
h :	0.0625
L2 :	0.000658294
H1 :	0.00415606
f :	0.466661

Iterative refinement
Refinement : 5
h :	0.03125
L2 :	0.000658294
H1 :	0.00415604
f :	0.466661
#############################################
Errors A-C (with iter ref):
#############################################
Iterative refinement
Refinement : 0
h :	1
L2 :	0.00258827
H1 :	0.0284073
f :	0.466669

Iterative refinement
Refinement : 1
h :	0.5
L2 :	0.000721468
H1 :	0.00766126
f :	0.466661

Iterative refinement
Refinement : 2
h :	0.25
L2 :	0.000642952
H1 :	0.00415285
f :	0.466661

Iterative refinement
Refinement : 3
h :	0.125
L2 :	0.000642378
H1 :	0.00405445
f :	0.466661

Iterative refinement
Refinement : 4
h :	0.0625
L2 :	0.000642373
H1 :	0.00405287
f :	0.466661

Iterative refinement
Refinement : 5
h :	0.03125
L2 :	0.000642373
H1 :	0.00405284
f :	0.466661

#############################################
Errors A-C (without iter ref):
#############################################
Refinement : 0
h :	1
L2 :	0.00254262
H1 :	0.0282073
f :	0.466669

Refinement : 1
h :	0.5
L2 :	0.000329561
H1 :	0.00651976
f :	0.466661

Refinement : 2
h :	0.25
L2 :	2.22262e-05
H1 :	0.000912305
f :	0.466661

Refinement : 3
h :	0.125
L2 :	1.36104e-06
H1 :	0.000115686
f :	0.466661

Refinement : 4
h :	0.0625
L2 :	8.27777e-08
H1 :	1.44013e-05
f :	0.466661

Refinement : 5
h :	0.03125
L2 :	5.07673e-09
H1 :	1.7908e-06
f :	0.466661

#############################################
Errors A+C (without iter ref):
#############################################
Refinement : 0
h :	1
L2 :	0.00273992
H1 :	0.0289772
f :	0.466669

Refinement : 1
h :	0.5
L2 :	0.00126894
H1 :	0.0103387
f :	0.466661

Refinement : 2
h :	0.25
L2 :	0.00122334
H1 :	0.00814395
f :	0.466661

Refinement : 3
h :	0.125
L2 :	0.00122288
H1 :	0.00809684
f :	0.466661

Refinement : 4
h :	0.0625
L2 :	0.00122287
H1 :	0.00809612
f :	0.466661

Refinement : 5
h :	0.03125
L2 :	0.00122287
H1 :	0.0080961
f :	0.466661
